﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp140
{
   public static class DifferenceOfSquares
    {
        static void Main(string[] args)
        {
           
        }
        public static int CalculateSquareOfSum(int max) => (int)Math.Pow((1 + max) * max / 2, 2);

        public static int CalculateSumOfSquares(int max) => Enumerable.Range(1, max).Select(x => x * x).Sum();

        public static int CalculateDifferenceOfSquares(int max)
            => CalculateSquareOfSum(max) - CalculateSumOfSquares(max);
    }
}
